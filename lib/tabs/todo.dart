import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../models/tasks.dart';

class Todo extends StatefulWidget {

  @override
  _TodoState createState() => _TodoState();

}


class _TodoState extends State<Todo> {

   List<Task> tasks = [
     Task('Créer une classe TaskList',
          'Remplacer la liste de Task par un type à part entière',
          false),
     Task('Créer une classe TaskList',
          'Remplacer la liste de Task par un type à part entière',
          true),
     Task('Créer une classe TaskList',
          'Remplacer la liste de Task par un type à part entière',
          false),
   ];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Liste de tâches',
          style: Theme.of(context).textTheme.headline4),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width * 0.8,
              child: TextField(
                decoration: InputDecoration(
                  labelText: 'Ajouter une tâche',
                ),
              ),
            ),

            Container(
              height: 50,
              width: MediaQuery.of(context).size.width * 0.19,
              child: ElevatedButton(
                child: Text("Ajouter"),
                onPressed: () {}
              )
            )

          ],
        ),
        Container(
          height: 400,
          child: ListView.builder(
            itemCount: tasks.length,
            itemBuilder: (BuildContext context, int index) {
              Task task = tasks[index];
              return ListTile(
                tileColor: Colors.deepPurple,
                leading: new CircleAvatar(
                backgroundColor: Colors.indigoAccent,
                child: new Text('Todo'),
                foregroundColor: Colors.white,
              ),
                contentPadding: EdgeInsets.fromLTRB(15, 5, 30, 5),
                title: new Text(task.label),
                trailing: task.isDone? Icon(Icons.done): null,
                subtitle: new Text(task.description),
                onTap: () => {
                  setState(() {
                    task.isDone = !task.isDone;
                    task.isDone? tasks.removeAt(0): null;
                  })
                },

              );
            }
          )

        ),
      //  FloatingActionButton(
      //   onPressed: () => {
      //     http.post(
      //       Uri.parse('http://asciiparait.fr:5000/zenidoc/1'),
      //       headers:{ "content-type": "application/json" },
      //       body: jsonEncode({
      //         "nb": tasks.length,
      //       })).then((res) => {
      //
      //     })
      //   },
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // )
      ],
    );
  }

}
