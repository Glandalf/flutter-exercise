# Exercice - débugguer un code existant

> Cet exercice est décomposé en trois paliers de difficulté. Il n'est pas nécessaire de réaliser les trois niveaux : si vous bloquez sur un palier, vous pouvez passer à l'exercice suivant malgré tout. Le principe est de résoudre les bugs / manques actuels. La manière de résoudre n'est pas particulièrement contrainte : il s'agit de faire fonctionner l'application, d'une manière que vous jugerez pertinente.


## Contexte

Nous avons une liste de tâches qui fonctionnait correctement mais dans la précipitation de la dernière mise à jour et face au manque de maitrise de la technologie, plusieurs développeurs ont fait des modifications qui ont cassé certaines fonctionnalités.
Il faudrait donc réparer certaines choses dans la mesure du possible.


## Tâches simples

* Enlever la couleur de fond des éléments de la liste (devraient être blancs au lieu de mauve)
* Le cercle de chaque tâche devrait apparaitre de la même couleur que les autres éléments du design
* Lorsque l'on clique sur une tâche, il arrive qu'une tâche disparaisse de la liste
* Lorsqu'une tâche est terminée, une "coche" apparait. Il faudrait que lorsque la tâche n'est pas cochée on affiche l'icône "outlet" inclue dans le package d'icônes standard de Material


## Tâches intermédiaires

* Le formulaire de saisie d'une tâche devrait ajouter la tâche dans la liste et la faire apparaitre
* Il faudrait ajouter un bouton ou une icône sur chaque tâche pour pouvoir supprimer la tâche
* Il faudrait remplacer le "Todo" qui apparait dans chaque tâche par la première lettre du label
* Les tâches terminées devraient apparaitre en "moins contrasté", écrites en gris clair par exemple


## Tâches avancées

* les tâches devraient apparaitre de manière triée : les tâches non terminées en haut, les autres en bas
* Lorsque l'on enregistre une tâche, elle devrait aussi être enregistrée sur notre serveur via notre API HTTP externe (voir annexe)
* Lorsque l'on charge le widget de TodoList, il faudrait initialiser les tâches avec celles enregistrées sur notre serveur via notre API HTTP externe (voir annexe)
* Le bouton `add` dans `main` devrait afficher/masquer le formulaire de saisie d'une nouvelle tâche

Optionnel : si vous n'avez pas pu traiter les points ci-dessus, vous pourriez essayer de traiter le suivant.

* Lorsque l'on swipe une tâche, on voudrait afficher un bouton de suppression (et au clic supprimer effectivement la tâche)


## Annexe - API externe

Nous disposons d'un entrepôt de stockage d'objets JSON de toutes sortes. En l'état cette API nous permet de :
* stocker tous types d'objets JSON dans des "collections" de notre choix
* récupérer tous les objets que l'on a stocké dans une de ces collections


Pour enregistrer un objet dans la collection `<votre_nom>-tasks`, par exemple :

```http
POST http://asciiparait.fr:5000/zenidoc/<votre_nom>-tasks
Content-Type: application/json

{
  "my first key": "its value",
  "number value": 12,
  "boolean value": true
}

```




Pour récupérer tous les objets stockés dans la même collection :

```http
GET http://asciiparait.fr:5000/zenidoc/<votre_nom>-tasks

```

